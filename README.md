# Diredt Debit Service
##### The project was created using the following curl command:


`curl https://start.spring.io/starter.zip \
      -d dependencies=web,security \
      -d type=gradle-project \
      -d bootVersion=2.2.0.RELEASE \
      -d groupId=com.leafbank.directdebit \
      -d artifactId=application \
      -o DirectDebit.zip
    unzip DirectDebit.zip `


##Spring Security Filter chains:

Spring security adds a lot of filters. A filter is a component that gets invoked automatically when 
a request arrives at the application server.

The filter can do a lot of things to the incoming request. It can add new headers or
remove existing ones. 

Once the filters job is done it can either pass the request to the NEXT filter in the 
filter chain or return a response.

It can also block the request from getting any more processing done.

Once the request has gone down the filter chain, it then reaches the controller method
via the dispatcher servlet.

####Request flow - no authentication 

`request -> pass through all filters as no auth -> AbstractSecurityInterceptor & AccessDecisionManager will determine
 target resource method`

the security configurtion determins this `no aouthetication flow` should be allowed to proceed to the sais resource.

####Request flow - require authentication

`request -> filters -> `
1.AbstractSecurityInterceptor throws AuthenticationException
2.Exception caught by ExceptionTranslationFilter
3.If after validating, it determines it is not allowed to proceed, then delegates to `AuthenticationEntryPoint component`
This then thow a http 401

#####But if auth credentials are in the header.......
extract credentails

build an `Authentication` object

This Authentication is passed to `AuthenticationManager` that asks its configured and attached `AuthenticationProviders`, 
if any of them can process such type of Authentication (say, UsernamePasswordAuthentication)
If yes, authenticate with database records.
If validation is succesfull,  a new output `Authentication` object is created. 
This output object is marked as successfully authenticated and 
filled with `GrantedAuthorities` that corresponds to the roles assigned to this particular user.

The best part is that....

The filter that initiated this operation will put this `Authentication` to `SecurityContextHolder`
 and pass the request down to the next filters. 
 Any further filters will check if `SecurityContextHolder` holds valid `Authentication` 
 and use `GrantedAuthorities` to do authorisation validation.


#### Setting up MYSQL as a Docker Container based DB
You need to download the Mysql Docker image for this.

` docker run \
 --name mysql_direct_debit_service \
 -h 127.0.0.1 \
 -p 3306:3306 \
 -e MYSQL_ALLOW_EMPTY_PASSWORD=true \
 -e MYSQL_DATABASE=direct_debit_service \
 -d mysql
 `
 For Postgres, You need to download the Postgres Docker image for this.
  docker run \
  --name pg_direct_debit_service \
  -h 127.0.0.1 \
  -p 5432:5432 \
  -e POSTGRES_PASSWORD=postgres \
  -e POSTGRES_DB=direct_debit_service \
  -d postgres
 
  To go into the container type : docker exec -it postgres-docker bash
  And with exec we’ve entered a postgres-docker image in detached mode -it and started to run it’s bash app (bash).
 
  login to the database as a postgres user.
  to start postgres inside the container : 
 
 ##Postgres Commands you need: courtesy: 
 ###https://www.postgresqltutorial.com/psql-commands/
 1) Connect to PostgreSQL database
 The following command connects to a database under a specific user. After pressing Enter PostgreSQL will ask for the password of the user.
 
 psql -d database -U  user -W
 For example, to connect to dvdrental database under postgres user, you use the following command:
 
 C:\Program Files\PostgreSQL\9.5\bin>psql -d dvdrental -U postgres -W
 Password for user postgres:
 dvdrental=#
 If you want to connect to a database that resides on another host, you add the -h option as follows:
 
 psql -h host -d database -U user -W
 In case you want to use SSL mode for the connection, just specify it as shown in the following command:
 
 psql -U user -h host "dbname=db sslmode=require"
 2) Switch connection to a new database
 Once you are connected to a database, you can switch the connection to a new database under a user specified by user. The previous connection will be closed. If you omit the user parameter, the current user is assumed.
 
 \c dbname username
 The following command connects to dvdrental database under postgres user:
 
 postgres=# \c dvdrental
 You are now connected to database "dvdrental" as user "postgres".
 dvdrental=#
 3) List available databases
 To list all databases in the current PostgreSQL database server, you use \l command:
 
 \l
 4) List available tables
 To list all tables in the current database, you use \dt command:
 
 \dt
 Note that this command shows the only table in the currently connected database.
 
 5) Describe a table
 To describe a table such as a column, type, modifiers of columns, etc., you use the following command:
 
 \d table_name
 6) List available schema
 To list all schemas of the currently connected database, you use the \dn command.
 
 \dn
 7) List available functions
 To list available functions in the current database, you use the \df command.
 
 \df
 8) List available views
 To list available views in the current database, you use the \dv command.
 
 \dv
 9) List users and their roles
 To list all users and their assign roles, you use \du command:
 
 \du
 10) Execute the previous command
 To retrieve the current version of PostgreSQL server, you use the version() function as follows:
 
 SELECT version();
 Now, you want to save time typing the previous command again, you can use \g command to execute the previous command:
 
 \g
 psql executes the previous command again, which is the SELECT statement,.
 
 11) Command history
 To display command history, you use the \s command.
 
 \s
 If you want to save the command history to a file, you need to specify the file name followed the \s command as follows:
 
 \s filename
 12) Execute psql commands from a file
 In case you want to execute psql commands from a file, you use \i command as follows:
 
 \i filename
 13) Get help on psql commands
 To know all available psql commands, you use the \? command.
 
 \?
 To get help on specific PostgreSQL statement, you use the \h command.
 
 For example, if you want to know detailed information on ALTER TABLE statement, you use the following command:
 
 \h ALTER TABLE
 14) Turn on query execution time
 To turn on query execution time, you use the \timing command.
 
 dvdrental=# \timing
 Timing is on.
 dvdrental=# select count(*) from film;
  count
 -------
   1000
 (1 row)
 
 Time: 1.495 ms
 dvdrental=#
 You use the same command \timing to turn it off.
 
 dvdrental=# \timing
 Timing is off.
 dvdrental=#

 `source https://medium.com/@backslash112/start-a-remote-mysql-server-with-docker-quickly-9fdff22d23fd`
 
 ##Gradle
 `The Gradle build system uses plug-ins to extend its core functionality. A plug-in is an extension to Gradle which typically adds some preconfigured tasks. Gradle ships with a number of plug-ins, and you can develop custom plug-ins.`
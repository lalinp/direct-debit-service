#!/bin/sh

# This script runs the unit tests in BitBucket. This is called in the step `Unit Tests`. Please see this fie
# Referenced in the `bitbucket.pipelines.yml`
# The unit tests might require a database so, we will provide on for EVERY build. We will use APT (Advanced Package Tool) Linux
# command for this.

:'
APT (Advanced Package Tool) is the command line tool to interact with this packaging system.
There are already dpkg commands to manage it, but apt is a more user-friendly way to handle packages.
You can use it to find and install new packages, upgrade packages, clean your packages, etc.
There are two main tools around APT: apt-get and apt-cache.
apt-get is for installing, upgrading, and cleaning packages
apt-cache is used for finding new packages

The Gradle build cache is a cache mechanism that aims to save time by reusing outputs produced by other builds.
The build cache works by storing (locally or remotely)
build outputs and allowing builds to fetch these outputs from the cache when
it is determined that inputs have not changed, avoiding the expensive work of regenerating them.
'
# apt-get update && apt-get install default-mysql-client -y
#mysql -h 127.0.0.1 clean unitTest

./gradlew --build-cache clean integrationTest


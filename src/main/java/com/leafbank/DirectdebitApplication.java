package com.leafbank;

import com.leafbank.directdebit.model.DirectDebitApplicationProperties;
import com.leafbank.directdebit.model.DirectDebitAwsPublisherProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties({DirectDebitApplicationProperties.class, DirectDebitAwsPublisherProperties.class})
public class DirectdebitApplication {

	public static void main(String[] args) {
		SpringApplication.run(DirectdebitApplication.class, args);
	}

}

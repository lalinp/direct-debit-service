package com.leafbank.directdebit.api;

import com.leafbank.directdebit.repository.DirectDebitDto;
import com.leafbank.directdebit.service.DirectDebitService;
import io.swagger.annotations.ApiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/directdebits")
@ApiModel(description = "Controller API for Direct Debit related CRUD Operations")
public class DirectDebitController {

  private final DirectDebitService directDebitService;

  @Autowired
  public DirectDebitController(DirectDebitService directDebitService) {

    this.directDebitService = directDebitService;
  }

  @GetMapping
  public String getDirectDebitDetails() {

    directDebitService.saveDirectDebit();
    return "done";
  }

  @GetMapping("/{id}")
  public ResponseEntity<DirectDebitDto> getDirectDebitById(@PathVariable Long id) {

    DirectDebitDto directDebit = directDebitService.findById(id);
    return ResponseEntity.ok().body(directDebit);
  }
}

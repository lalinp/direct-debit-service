package com.leafbank.directdebit.config;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultSecurityFilterChain;

/**
 * The @EnableGlobalMethodSecurity(prePostEnabled = true) annotation is what enables
 * the @PreAuthorize annotation. This can be added to any class with the @Configuration annotation.
 * You can also enable @Secured, an older SpringSecurity annotation, and JSR-250 annotations.
 *
 * <p>The configure(final HttpSecurity http) method overrides the default HttpBuilder configuration.
 * If it’s empty, it leaves the application without authorization or authentication.
 *
 * No need to use {@Depricated @EnableWebMvcSecurity} anymore.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(1)
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String UNAUTHORISED_MESSAGE = "Your request is unauthorised. Did you "
      + "forget the JWT in your Authorization header?";

  private final SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> configurerAdapter;

  @Autowired
  public ApiSecurityConfig(
      SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> configurerAdapter) {
    this.configurerAdapter = configurerAdapter;
  }

  protected void configure(final HttpSecurity http) throws Exception{

    http.antMatcher("*").anonymous()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling().authenticationEntryPoint(unauthorisedEntryPoint())
    .and().apply(configurerAdapter);
  }

  @Bean
  public AuthenticationEntryPoint unauthorisedEntryPoint(){
    return ((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED, UNAUTHORISED_MESSAGE));
  }

}

package com.leafbank.directdebit.config;

import com.leafbank.directdebit.model.DirectDebitApplicationProperties;
import com.leafbank.directdebit.model.DirectDebitAwsPublisherProperties;
import java.time.Clock;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.leafbank.directdebit")
@EnableConfigurationProperties({DirectDebitApplicationProperties.class,
    DirectDebitAwsPublisherProperties.class})
public class AppConfig {

  @Bean
  Clock clock(){
    return Clock.systemUTC();
  }

}

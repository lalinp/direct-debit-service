package com.leafbank.directdebit.config;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.leafbank.directdebit.model.DirectDebitAwsPublisherProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AwsConfig {

  private final DirectDebitAwsPublisherProperties directDebitAwsProperties;

  @Autowired
  public AwsConfig(DirectDebitAwsPublisherProperties directDebitAwsProperties) {
    this.directDebitAwsProperties = directDebitAwsProperties;
  }

  @Bean
  @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
  public AmazonSNS awsSnsClientBuilder(){
    return AmazonSNSClient.builder()
        .withRegion(directDebitAwsProperties.getAwsRegion())
        .withCredentials(new ProfileCredentialsProvider())
        .build();
  }
  @Bean
  public NotificationMessagingTemplate notificationMessagingTemplate(AmazonSNS amazonSNSClient){
    return new NotificationMessagingTemplate(amazonSNSClient);
  }
}

package com.leafbank.directdebit.config;


import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrpcConfig {

  @Value("${leaf.grpc.accountService.port}")
  private int port;

  @Value("${leaf.grpc.accountService.host}")
  private String host;

  @Value("${leaf.grpc.keepAlive.frequency}")
  private int timeOutFrequency;

  @Value("${leaf.grpc.keepAlive.timeOut}")
  private int timeout;


 @Bean
  public ManagedChannel accountServiceClientChannel(){
    ManagedChannel accountServiceChannel = ManagedChannelBuilder.forAddress("localhost", 6565)
        .usePlaintext()
        .build();
    return accountServiceChannel;
    }
}

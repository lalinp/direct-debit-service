package com.leafbank.directdebit.config;

import com.leafbank.directdebit.model.DirectDebitApplicationProperties;
import com.leafbank.directdebit.security.JWTDecoder;
import com.leafbank.directdebit.security.JWTSecurityConfigurerAdapter;
import com.leafbank.directdebit.security.JwtTokenAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * This class
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityConfig {

  @Autowired
  DirectDebitApplicationProperties directDebitApplicationProperties;

  @Bean
  public JWTDecoder jwtDecoder(){

    return new JWTDecoder(directDebitApplicationProperties.getJwtSecret());
  }

  @Bean
  public JwtTokenAuthenticationFilter filter(JWTDecoder decoder){

    return new JwtTokenAuthenticationFilter(decoder);
  }
  @Bean
  public JWTSecurityConfigurerAdapter configurerAdapter(JwtTokenAuthenticationFilter filter){

  return new JWTSecurityConfigurerAdapter(filter);
  }


}

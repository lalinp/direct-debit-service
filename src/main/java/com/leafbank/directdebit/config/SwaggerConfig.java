package com.leafbank.directdebit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

  //basic swagger confg
  @Bean
  public Docket apiDocket(){
    Docket docket = new Docket(DocumentationType.SWAGGER_2)
        .useDefaultResponseMessages(false)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.leafbank.directdebit.api"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(getApiInfo());

    return docket;
  }

  private ApiInfo getApiInfo() {

    var apiInfo =
        new ApiInfoBuilder()
            .title("Direct-Debit-Service")
            .description("The Direct Debit Service API")
            .version("1.0")
            .license(null)
            .licenseUrl(null)
            .termsOfServiceUrl(null)
            .build();
    return apiInfo;
    }
}

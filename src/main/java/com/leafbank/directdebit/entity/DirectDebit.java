package com.leafbank.directdebit.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Table(name = "direct_debit")
@Data
public class DirectDebit implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private long id;

  @Column(name = "recipient_name", nullable = false)
  private String recipientName;

  @Column(name = "recipient_account_number", nullable = false)
  private String recipientAccountNumber;

  @Column(name = "recipient_sort_code", nullable = false)
  private String recipientSortCode;

  @Column(name = "dd_payment_date")
  @NotNull
  @Basic
  private java.time.LocalDateTime ddPaymentDate;

  @Column(name = "dd_set_up_date", updatable = false, nullable = false)
  @Basic
  private java.time.LocalDateTime ddSetUpDate;

  @Column(name = "currency_code", updatable = false, nullable = false)
  private String currencyCode;

  @Column(name = "source_account_id", nullable = false)
  private String sourceAccountId;

  @Column(name = "routing_number", updatable = false, nullable = false)
  private String routingNumber;

  @Column(name = "frequency", nullable = false)
  private String frequency;

  @Column(name = "dd_type", updatable = false, nullable = false)
  private String ddType;

  @Column(name = "status")
  private String status;

  @Column(name = "created_on", nullable = false)
  @Basic
  private java.time.LocalDateTime createdOn;

  @Column(name = "updated_on", nullable = false)
  @Basic
  private java.time.LocalDateTime updatedOnOn;
}

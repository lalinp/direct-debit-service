package com.leafbank.directdebit.exception;

public class MessagePublishingException extends RuntimeException {

  public MessagePublishingException(String message, Throwable throwable){
    super(message, throwable);
  }

}

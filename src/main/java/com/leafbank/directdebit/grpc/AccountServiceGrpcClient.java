package com.leafbank.directdebit.grpc;


import com.leaf.grpc.common.client.JwtTokenClientInterceptor;
import com.leafbank.accounts.AccountServiceGrpc;
import com.leafbank.accounts.AccountServiceGrpc.AccountServiceBlockingStub;
import com.leafbank.accounts.UkAccountByIdRequest;
import com.leafbank.accounts.UkAccountResponse;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceGrpcClient{

  private ManagedChannel accountServiceChannel;

  public AccountServiceGrpcClient(ManagedChannel accountServiceChannel){
    this.accountServiceChannel = accountServiceChannel;
  }

  public UkAccountResponse getUkAccountByAccountId(long id) {
    JwtTokenClientInterceptor clientInterceptor = new JwtTokenClientInterceptor("Im a token");
    Channel channel = ClientInterceptors.intercept(accountServiceChannel, clientInterceptor);
    AccountServiceBlockingStub stub = AccountServiceGrpc.newBlockingStub(channel);
    UkAccountByIdRequest request = UkAccountByIdRequest.newBuilder().setAccountId(12).build();
    UkAccountResponse response = stub.getUkAccountByAccountId(request);
    return response;
  }
}

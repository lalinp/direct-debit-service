package com.leafbank.directdebit.model;

import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "leaf.service")
public class DirectDebitApplicationProperties {

  @NotNull private String jwtSecret;

  @NotNull private String subjectClaim;

  @NotNull private String issuerClaim;
}

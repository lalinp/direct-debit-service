package com.leafbank.directdebit.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@ConfigurationProperties(prefix = "leaf.aws.config")
public class DirectDebitAwsPublisherProperties {

  @NotBlank
  private String snsTopicArn;

  @NotBlank
  private String awsRegion;

  @NotNull
  private Integer snsClientTimeoutMs;

  @NotBlank
  private String messageType;
}

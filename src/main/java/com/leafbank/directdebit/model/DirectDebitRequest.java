package com.leafbank.directdebit.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel
@JsonInclude(Include.NON_EMPTY)
public class DirectDebitRequest {

  @ApiModelProperty(
      notes = "Unique identifier of the person. No two persons can have the same id.",
      example = "1",
      required = true,
      position = 0)
  private String accountID;

  @ApiModelProperty(
      notes = "Unique identifier of the person. No two persons can have the same id.",
      example = "1",
      required = true,
      position = 0)
  private String recipientAccountId;

  @ApiModelProperty(
      notes = "Unique identifier of the person. No two persons can have the same id.",
      example = "1",
      required = true,
      position = 0)
  private String sortCode;

  @ApiModelProperty(
      notes = "Unique identifier of the person. No two persons can have the same id.",
      example = "1",
      required = true,
      position = 0)
  private String recipientName;

  @ApiModelProperty(
      notes = "Unique identifier of the person. No two persons can have the same id.",
      example = "1",
      required = true,
      position = 0)
  private String recipientBank;

  @ApiModelProperty(
      notes = "Unique identifier of the person. No two persons can have the same id.",
      example = "1",
      required = true,
      position = 0)
  private String recipientBranch;

  private int amountInPence;
  private LocalDateTime startDate;
  private LocalDateTime setupDate;
}

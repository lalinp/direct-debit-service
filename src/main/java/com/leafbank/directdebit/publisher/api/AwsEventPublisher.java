package com.leafbank.directdebit.publisher.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.leafbank.directdebit.exception.MessagePublishingException;
import com.leafbank.directdebit.model.DirectDebitAwsPublisherProperties;
import com.leafbank.directdebit.util.EventPublisherEnum;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

/**
 * Class for publishing DD events and commands
 */
@Component
public class AwsEventPublisher {

  private static final Logger LOGGER = Logger.getAnonymousLogger();
  private static final String DIRECT_DEBIT = "direct_debit";
  private static final String MESSAGE_TYPE = "message_type";

  private final NotificationMessagingTemplate notificationMessagingTemplate;
  private final DirectDebitAwsPublisherProperties directDebitAwsPublisherProperties;

  @Autowired
  public AwsEventPublisher(
      NotificationMessagingTemplate notificationMessagingTemplate,
      DirectDebitAwsPublisherProperties directDebitAwsPublisherProperties) {
    this.notificationMessagingTemplate = notificationMessagingTemplate;
    this.directDebitAwsPublisherProperties = directDebitAwsPublisherProperties;
  }

  /**
   * Publishes an event to SNS.
   *
   * @param message The message to be published.
   * @return A message to confirm successful delivery.
   * @throws JsonProcessingException if json parsing fails.
   * @throws MessagingException if the message was not published to SNS.
   */
  public String publishEvent(Message message) {

    MimeType mimeType = MimeType.valueOf(directDebitAwsPublisherProperties.getMessageType());
    MessageHeaders messageHeaders =
        new MessageHeaders(
            Map.of(
                MESSAGE_TYPE,
                mimeType.toString(),
                DIRECT_DEBIT,
                EventPublisherEnum.CREATE.getCrudOp()));

    String serializedMessagePayload;
    try {
      serializedMessagePayload = new ObjectMapper().writeValueAsString(message);
      org.springframework.messaging.Message<String> snsMessage =
          MessageBuilder.createMessage(serializedMessagePayload, messageHeaders);

      notificationMessagingTemplate.send(
          directDebitAwsPublisherProperties.getSnsTopicArn(), snsMessage);

    } catch (JsonProcessingException | MessagingException e) {
      String errorMsg =
          String.format(
              "Message could not be published message Id:[%s], AccountId [%s], CompanyId [%s], userId: [%s]",
              message.getMessageId(),
              message.getAccountId(),
              message.getCompanyId(),
              message.getUserId());
      LOGGER.severe(errorMsg);
      throw new MessagePublishingException(errorMsg, e);
    }
    return "Message " + message.getMessageId() + " was successfully published";
  }
}

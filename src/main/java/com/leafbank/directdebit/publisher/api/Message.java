package com.leafbank.directdebit.publisher.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;

@Getter
@EqualsAndHashCode
@Value
public class Message {

  private UUID messageId;
  private LocalDateTime timestamp;
  private Integer userId;
  private Integer accountId;
  private Integer companyId;
  @JsonSerialize(using = PayloadSerializer.class)
  private Payload payload;


  @Builder
  public Message(Payload payload, Integer userId, Integer accountId, Integer companyId){
    this.messageId = UUID.randomUUID();
    this.timestamp = LocalDateTime.now();
    this.userId = userId;
    this.accountId = accountId;
    this.companyId = companyId;
    this.payload = payload;
  }
}

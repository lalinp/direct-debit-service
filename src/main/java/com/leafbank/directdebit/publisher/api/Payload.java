package com.leafbank.directdebit.publisher.api;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Payload {

  private UUID directDebitId;
  private String recipientAccountId;
  private String sortCode;
  private String recipientName;
  private String recipientBank;
  private String recipientBranch;
  private int amountInPence;
  private LocalDateTime startDate;
  private LocalDateTime setupDate;
}

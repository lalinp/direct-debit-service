package com.leafbank.directdebit.publisher.api;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Custom serializer for the payload
 */
public class PayloadSerializer extends StdSerializer<Payload> {

  public PayloadSerializer(){
    this(null);
  }

  public PayloadSerializer(Class<Payload> payloadClass) {
    super(payloadClass);
  }
  @Override
  public void serialize(Payload payload, JsonGenerator gen, SerializerProvider provider) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    JavaTimeModule module = new JavaTimeModule();
    DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
    LocalDateTimeSerializer dateTimeSerializer = new LocalDateTimeSerializer(formatter);
    module.addSerializer(LocalDateTime.class, dateTimeSerializer);
    /**
     * The JavaTimeModule enables the ObjectMapper to work with LocalDates
     * and the parameter WRITE_DATES_AS_TIMESTAMPS tells the mapper to represent a Date as a String in JSON.
     */
    mapper.registerModule(module);

    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    String payloadAsJson = mapper.writeValueAsString(payload);
    gen.writeString(payloadAsJson);
  }
}
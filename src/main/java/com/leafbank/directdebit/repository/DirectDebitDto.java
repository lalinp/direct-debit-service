package com.leafbank.directdebit.repository;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DirectDebitDto {

  private long directDebitId;

  private String recipientName;

  private String recipientAccountNumber;

  private String recipientSortCode;

  private java.time.LocalDateTime ddPaymentDate;

  private java.time.LocalDateTime ddSetUpDate;

  private String currencyCode;

  private String sourceAccountId;

  private String routingNumber;

  private String frequency;

  private String ddType;

  private String status;
}

package com.leafbank.directdebit.repository;

import com.leafbank.directdebit.entity.DirectDebit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface DirectDebitRepository extends JpaRepository<DirectDebit, Long> {

}

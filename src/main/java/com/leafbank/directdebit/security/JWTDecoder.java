package com.leafbank.directdebit.security;

import com.nimbusds.jose.JWSAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;

/** Class to decode an incoming JOSE JWT for authentication purposes */
public class JWTDecoder {

  private static final JWSAlgorithm SIGNING_ALGO = JWSAlgorithm.PS256;
  private final String jwtSecret;

  @Autowired
  public JWTDecoder(
      String jwtSecret)  {

    this.jwtSecret = jwtSecret;
  }
}

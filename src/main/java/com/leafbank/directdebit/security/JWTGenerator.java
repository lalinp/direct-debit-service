package com.leafbank.directdebit.security;

import com.leafbank.directdebit.model.DirectDebitApplicationProperties;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jwt.JWTClaimsSet;
import java.text.ParseException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.stereotype.Component;

/** Class to generate a JOSE JWT for authentication purposes */
@Component
public class JWTGenerator {

  private static final JWSAlgorithm SIGNING_ALGO = JWSAlgorithm.PS256;
  private final Clock clock;
  private final DirectDebitApplicationProperties directDebitApplicationProperties;
  //private final RSAKey rsaKey;
 // private final RSASSASigner signer;
  private final JWSHeader header;

  @Autowired
  public JWTGenerator(
      DirectDebitApplicationProperties directDebitApplicationProperties, Clock clock) throws ParseException, JOSEException {

    this.clock = clock;
    this.directDebitApplicationProperties = directDebitApplicationProperties;
    //rsaKey = RSAKey.parse(directDebitApplicationProperties.getJwtSecret());
    //signer = new RSASSASigner(rsaKey);
    header = buildHeader();

  }

  /**
   * Generate the signed JWT
   *
   * @return String The String representation of the JWT
   * @throws InternalAuthenticationServiceException
   */
  public String generateSignedJwt(){

    JWSObject jwsObject = new JWSObject(buildHeader(),buildPayload());
   /* try {
      jwsObject.sign(signer);
    } catch (JOSEException e) {
      throw new InternalAuthenticationServiceException("cannot sign the JWT");
    }*/

    return jwsObject.serialize();
  }

  /**
   * Builds the header
   *
   * @return JWSHeader returns a JWSHeader object
   */
  private JWSHeader buildHeader(){

    return new JWSHeader.Builder(SIGNING_ALGO)
        .contentType("json")
        .type(new JOSEObjectType("JOSE"))
        //.keyID(rsaKey.toString())
        .build();
  }

  /**
   * This method builds the payload with the claims set
   *
   * @return a Payload object
   */
  private Payload buildPayload(){

    JWTClaimsSet claimsSet =
        new JWTClaimsSet.Builder()
            .subject(directDebitApplicationProperties.getSubjectClaim())
            .issuer(directDebitApplicationProperties.getIssuerClaim())
            .expirationTime(getExpirationTime())
            .issueTime(getIssuerTime())
            .jwtID(UUID.randomUUID().toString())
            .build();

    return new Payload(claimsSet.toJSONObject());
  }

  /**
   * This method generates the issuer time for the JWT. Its the current Date and Time.
   *
   * @return Date returns a Date isntance of the issuer time
   */
  private Date getIssuerTime() {

    var now = LocalDateTime.now(clock).toInstant(ZoneOffset.UTC);

    return Date.from(now);
  }

  /**
   * This method generates the exp tie for the JWT.
   * At present, its set to 5 minuets foe testing purposes.
   *
   * @return Date a Date instance plus 5 minuets
   */
  private Date getExpirationTime() {

    var exp = LocalDateTime.now(clock).plusMinutes(5).toInstant(ZoneOffset.UTC);

    return Date.from(exp);
  }
}

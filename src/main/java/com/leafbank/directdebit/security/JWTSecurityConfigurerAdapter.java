package com.leafbank.directdebit.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * The <b>configuerer takes a filter instance</b>, then adds it to the filter chain BEFORE
 * UsernamePasswordAuthenticationFilter. This way, the filter is now available down the filter chain when the
 * UsernamePasswordAuthenticationFilter requires it.
 */
public class JWTSecurityConfigurerAdapter extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

  private final JwtTokenAuthenticationFilter filter;

  @Autowired
  public JWTSecurityConfigurerAdapter(JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter){
    this.filter = jwtTokenAuthenticationFilter;
  }

  @Override
  public void configure(HttpSecurity httpSecurity){

    httpSecurity.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
  }
}

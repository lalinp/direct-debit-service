package com.leafbank.directdebit.security;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.GenericFilterBean;

public class JwtTokenAuthenticationFilter extends GenericFilterBean {

  private static final String AUTHORIZATION_HEADER = "Authorization";

  private final JWTDecoder jwtDecoder;

  @Autowired
  public JwtTokenAuthenticationFilter(JWTDecoder jwtDecoder) {
    this.jwtDecoder = jwtDecoder;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest httpRequest = (HttpServletRequest) request;

    String bearerTokenAsString = httpRequest.getHeader(AUTHORIZATION_HEADER);

    chain.doFilter(request,response);
    }
}

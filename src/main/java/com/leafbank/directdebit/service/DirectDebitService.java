package com.leafbank.directdebit.service;

import com.leafbank.accounts.UkAccountResponse;
import com.leafbank.directdebit.entity.DirectDebit;
import com.leafbank.directdebit.exception.EmployeeNotFoundException;
import com.leafbank.directdebit.grpc.AccountServiceGrpcClient;
import com.leafbank.directdebit.model.DirectDebitAwsPublisherProperties;
import com.leafbank.directdebit.publisher.api.AwsEventPublisher;
import com.leafbank.directdebit.publisher.api.Message;
import com.leafbank.directdebit.publisher.api.Payload;
import com.leafbank.directdebit.repository.DirectDebitDto;
import com.leafbank.directdebit.repository.DirectDebitRepository;
import java.time.LocalDateTime;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectDebitService {

  private final DirectDebitAwsPublisherProperties directDebitProperties;
  private final AwsEventPublisher awsEventPublisher;
  private final AccountServiceGrpcClient client;
  private final DirectDebitRepository repository;

  @Autowired
  public DirectDebitService(
      DirectDebitAwsPublisherProperties directDebitProperties,
      AwsEventPublisher awsEventPublisher,
      AccountServiceGrpcClient client,
      DirectDebitRepository repository) {
    this.directDebitProperties = directDebitProperties;
    this.awsEventPublisher = awsEventPublisher;
    this.client = client;
    this.repository = repository;
  }

  public String setUpDirectDebit(String payload){

    //publish to topic when you've successfully persisted the record
    publishDirectDebitEvent(Message.builder().build());
    return "all done";
  }

  public String getResult() {
    return awsEventPublisher.publishEvent(messageBuilder());
  }

  public String publishDirectDebitEvent( Message message){
    return awsEventPublisher.publishEvent(message);
  }

  private Message messageBuilder(){
    com.leafbank.directdebit.publisher.api.Message message =
        com.leafbank.directdebit.publisher.api.Message.builder()
            .accountId(1234556)
            .companyId(3456)
            .userId(6543)
            .payload(payloadForSns())
            .build();

    return message;
  }

  private Payload payloadForSns() {
    return Payload.builder()
        .directDebitId(UUID.randomUUID())
        .recipientAccountId("26082055")
        .sortCode("866084")
        .recipientBank("Natwest")
        .recipientBranch("Guildford")
        .recipientName("Augustus Gloop the nincompoop")
        .amountInPence(20000)
        .setupDate(LocalDateTime.now())
        .startDate(LocalDateTime.now().minusMonths(1))
        .build();
  }

  public void saveDirectDebit(){
    DirectDebit dd = new DirectDebit();
    dd.setCreatedOn(LocalDateTime.now());
    dd.setUpdatedOnOn(LocalDateTime.now());
    dd.setDdType("DD");
    dd.setCurrencyCode("GBP");
    dd.setDdPaymentDate(LocalDateTime.now());
    dd.setFrequency("Monthly");
    dd.setDdPaymentDate(LocalDateTime.now());
    dd.setRecipientAccountNumber("12345");
    dd.setRecipientName("abd");
    dd.setRecipientSortCode("123456");
    dd.setDdSetUpDate(LocalDateTime.now());
    dd.setRoutingNumber("1");
    dd.setSourceAccountId("54321");
    repository.save(dd);
  }

  public DirectDebitDto findById(long id){

    UkAccountResponse response = client.getUkAccountByAccountId(id);
     return repository.findById(id)
         .map(dd -> toDto(dd))
         .orElseThrow(() -> new EmployeeNotFoundException("Record not found"));
  }

  private DirectDebitDto toDto(DirectDebit dd){
    return DirectDebitDto.builder()
        .currencyCode(dd.getCurrencyCode())
        .ddPaymentDate(dd.getDdPaymentDate())
        .ddSetUpDate(dd.getDdSetUpDate())
        .ddType(dd.getDdType())
        .directDebitId(dd.getId())
        .frequency(dd.getFrequency())
        .recipientAccountNumber(dd.getRecipientAccountNumber())
        .recipientName(dd.getRecipientName())
        .recipientSortCode(dd.getRecipientSortCode())
        .routingNumber(dd.getRoutingNumber())
        .sourceAccountId(dd.getSourceAccountId())
        .status(dd.getStatus()==null?"active": dd.getStatus())
        .build();
  }

}

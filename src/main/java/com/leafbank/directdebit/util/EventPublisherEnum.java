package com.leafbank.directdebit.util;

public enum EventPublisherEnum {
  CREATE("create"),
  UPDATE("update"),
  DELETE("delete");

  public final String crudOp;

  private EventPublisherEnum(String crudOp) {
    this.crudOp = crudOp;
  }

  public String getCrudOp(){
    return this.crudOp;
  }
}

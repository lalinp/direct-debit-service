package com.leafbank.directdebit.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import({AppConfig.class})
@ComponentScan(basePackages = "com.leafbank")
public class IntegrationTestConfig {

}

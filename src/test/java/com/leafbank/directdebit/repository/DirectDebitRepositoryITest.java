package com.leafbank.directdebit.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.leafbank.directdebit.config.IntegrationTestConfig;
import com.leafbank.directdebit.entity.DirectDebit;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(classes = {IntegrationTestConfig.class})
@ExtendWith(SpringExtension.class)

public class DirectDebitRepositoryITest {

  private static final String SOURCE_ACCOUNT_ID = "123456";
  private static final String ROUTING_NUMBER = "1";
  private static final String RECIPIENT_NAME = "Georgia Pethiyagoda";
  private static final String RECIPIENT_ACCOUNT_NUMBER = "87654321";
  private static final String RECIPIENT_SORT_CODE = "543210";
  private static final String SOURCE_ACCOUNT_ID1 = "654321";
  private static final String FREQUENCY = "MONTHLY";
  private static final String DD_TYPE = "DD";
  private static final String STATUS_ACTIVE = "ACTIVE";
  private static final String CURRENCY_CODE = "GBP";

  @Autowired
  private DirectDebitRepository directDebitRepository;


  @Test
  public void saveDirectDebit_returnSavedRecord(){
    DirectDebit dd = getDirectDebit();
    var savedDirectDebit = directDebitRepository.save(dd);
    assertEquals(dd.getRecipientName(), savedDirectDebit.getRecipientName());
    assertEquals(dd.getDdType(), savedDirectDebit.getDdType());
    assertEquals(dd.getCurrencyCode(), savedDirectDebit.getCurrencyCode());
    assertEquals(dd.getCreatedOn(), savedDirectDebit.getCreatedOn());
    assertEquals(dd.getDdPaymentDate(), savedDirectDebit.getDdPaymentDate());
    assertEquals(dd.getDdSetUpDate(), savedDirectDebit.getDdSetUpDate());
    assertEquals(dd.getFrequency(), savedDirectDebit.getFrequency());
    assertEquals(dd.getRecipientAccountNumber(), savedDirectDebit.getRecipientAccountNumber());
    assertEquals(dd.getRecipientSortCode(), savedDirectDebit.getRecipientSortCode());
    assertEquals(dd.getSourceAccountId(), savedDirectDebit.getSourceAccountId());
    assertEquals(dd.getStatus(), savedDirectDebit.getStatus());
    assertNotNull(savedDirectDebit.getId());
  }

  private DirectDebit getDirectDebit(){
    DirectDebit dd = new DirectDebit();
    dd.setSourceAccountId(SOURCE_ACCOUNT_ID);
    dd.setRoutingNumber(ROUTING_NUMBER);
    dd.setDdSetUpDate(LocalDateTime.now());
    dd.setRecipientName(RECIPIENT_NAME);
    dd.setRecipientAccountNumber(RECIPIENT_ACCOUNT_NUMBER);
    dd.setRecipientSortCode(RECIPIENT_SORT_CODE);
    dd.setSourceAccountId(SOURCE_ACCOUNT_ID1);
    dd.setFrequency(FREQUENCY);
    dd.setDdType(DD_TYPE);
    dd.setDdPaymentDate(LocalDateTime.now());
    dd.setStatus(STATUS_ACTIVE);
    dd.setCreatedOn(LocalDateTime.now());
    dd.setUpdatedOnOn(LocalDateTime.now());
    dd.setCurrencyCode(CURRENCY_CODE);
    return dd;
  }
}
